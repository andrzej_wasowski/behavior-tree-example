// based on https://github.com/BehaviorTree/BehaviorTree.CPP/blob/master/examples/t01_build_your_first_tree.cpp
#include "behaviortree_cpp_v3/bt_factory.h"

using namespace BT;

static const char* xml_text = R"(

 <root main_tree_to_execute = "MainTree" >

     <BehaviorTree ID="MainTree">
        <Sequence name="root_sequence">
            <MyAction name="My Action #1"/>
            <MyAction name="My Action #2"/>
            <MyAction name="My Action #3"/>
            <MyAction name="My Action #4"/>
            <MyAction name="My Action #5"/>
        </Sequence>
     </BehaviorTree>

 </root>
 )";

class MyAction : public BT::SyncActionNode
{
  public:
    MyAction(const std::string& name) :
        BT::SyncActionNode(name, {}) { }

    BT::NodeStatus tick() override {
      std::cout << "ACTION: " << this->name() << std::endl;
      return BT::NodeStatus::SUCCESS;
    }
};


int main()
{
    BehaviorTreeFactory factory;
    factory.registerNodeType<MyAction>("MyAction");
    auto tree = factory.createTreeFromText(xml_text);

    // To "execute" a Tree you need to "tick" it.
    // The tick is propagated to the children based on the logic of the tree.
    // In this case, the entire sequence is executed, because all the children
    // of the Sequence return SUCCESS.
    tree.tickRoot();

    return 0;
}

/* 

Outputs:
 
ACTION: My Action #1
ACTION: My Action #2
ACTION: My Action #3
ACTION: My Action #4
ACTION: My Action #5

*/
